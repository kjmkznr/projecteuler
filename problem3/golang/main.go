package main

import (
	"fmt"
	"sort"
)

func divide(m, n int, primes *[]int) int {
	for m%n == 0 {
		*primes = append(*primes, n)
		m = m / n
	}
	return m
}

func largestPrimeFactor(number int) int {
	var primes []int

	n := divide(number, 2, &primes)
	i := 3
	for {
		n = divide(n, i, &primes)
		if n == 1 {
			break
		}
		i++
	}

	sort.Ints(primes)

	return primes[len(primes)-1]
}
func main() {
	fmt.Printf("%v\n", largestPrimeFactor(600851475143))
}
