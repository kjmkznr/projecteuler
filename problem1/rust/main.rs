fn sum(max: i32) -> i32 {
  let mut sum = 0;
  for i in 0..max {
    if i%3 == 0 || i%5 == 0 {
      sum += i
    }
  }

  return sum
}
fn main() {
  let x = sum(1000);
  println!("sum = {}", x);
}
