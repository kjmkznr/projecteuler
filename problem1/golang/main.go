package main

import "fmt"

func sum(max int) int {
	sum := 0
	for i := 0; i < max; i++ {
		if (i%3) == 0 || (i%5) == 0 {
			sum += i
		}
	}
	return sum
}

func main() {
	i := sum(1000)
	fmt.Println(i)
}
