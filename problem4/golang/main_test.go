package main

import "testing"

func TestIsPalindrome(t *testing.T) {
	if b := isPalindrome(1000); b {
		t.Errorf("isPalindrome returns incorrect value: got %v, expect %v", b, false)
	}
	if b := isPalindrome(1001); !b {
		t.Errorf("isPalindrome returns incorrect value: got %v, expect %v", b, true)
	}
	if b := isPalindrome(9009); !b {
		t.Errorf("isPalindrome returns incorrect value: got %v, expect %v", b, true)
	}
	if b := isPalindrome(9999); !b {
		t.Errorf("isPalindrome returns incorrect value: got %v, expect %v", b, true)
	}
	if b := isPalindrome(80008); !b {
		t.Errorf("isPalindrome returns incorrect value: got %v, expect %v", b, true)
	}
	if b := isPalindrome(82028); !b {
		t.Errorf("isPalindrome returns incorrect value: got %v, expect %v", b, true)
	}
}
