package main

import (
	"fmt"
	"sort"
	"strconv"
)

func isPalindrome(num int) bool {
	s := strconv.Itoa(num)

	l := len(s)

	for i := 0; i < l/2; i++ {
		if s[i] != s[l-i-1] {
			return false
		}
	}
	return true
}

func largestPalindromeProduct(digit int) (int, int) {
	min := 1
	for i := 1; i < digit; i++ {
		min *= 10
	}
	max := min*10 - 1

	candidate := make(map[int][]int)
	var candidateKeys []int
	for i := max; i >= min; i-- {
		for j := max; j >= min; j-- {
			if b := isPalindrome(i * j); b {
				candidate[i*j] = []int{i, j}
				candidateKeys = append(candidateKeys, i*j)
			}
		}
	}

	sort.Sort(sort.Reverse(sort.IntSlice(candidateKeys)))
	ans := candidate[candidateKeys[0]]
	return ans[0], ans[1]
}

func main() {
	{
		n1, n2 := largestPalindromeProduct(2)
		fmt.Printf("Largest Palindrome Product %d * %d = %d\n", n1, n2, n1*n2)
	}
	{
		n1, n2 := largestPalindromeProduct(3)
		fmt.Printf("Largest Palindrome Product %d * %d = %d\n", n1, n2, n1*n2)
	}
	{
		n1, n2 := largestPalindromeProduct(10)
		fmt.Printf("Largest Palindrome Product %d * %d = %d\n", n1, n2, n1*n2)
	}
}
