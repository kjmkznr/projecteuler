package main

import "fmt"

func evenFib(n int) int {
	a := 0
	b := 1
	sum := 0
	for b < n {
		if b%2 == 0 {
			sum += b
		}
		a, b = b, a+b
	}

	return sum
}

func fib() func() int {
	a, b := 0, 1
	return func() int {
		a, b = b, a+b
		return a
	}
}

func evenFib2(n int) int {
	sum := 0
	x := 0
	for f := fib(); x < n; x = f() {
		if x%2 == 0 {
			sum += x
		}
	}
	return sum
}

func main() {
	fmt.Println(evenFib(4000000))
	fmt.Println(evenFib2(4000000))
}
