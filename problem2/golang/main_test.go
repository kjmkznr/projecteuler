package main

import "testing"

func TestEvenFib(t *testing.T) {
	if sum := evenFib(100); sum != 44 {
		t.Errorf("Expect %d, Actual %d", 44, sum)
	}
	if sum := evenFib(4000000); sum != 4613732 {
		t.Errorf("Expect %d, Actual %d", 4613732, sum)
	}
}

func BenchmarkEvenFib(b *testing.B) {
	for i := 0; i < b.N; i++ {
		evenFib(i)
	}
}

func TestEvenFib2(t *testing.T) {
	if sum := evenFib2(100); sum != 44 {
		t.Errorf("Expect %d, Actual %d", 44, sum)
	}
	if sum := evenFib2(4000000); sum != 4613732 {
		t.Errorf("Expect %d, Actual %d", 4613732, sum)
	}
}

func BenchmarkEvenFib2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		evenFib2(i)
	}
}
