package main

import "fmt"

func problem5(n int) int {
	i := 0
	for {
		i += 10
		b := true
		for l := 1; l <= n; l++ {
			if i%l != 0 {
				b = false
			}
		}

		if b {
			break
		}
	}

	return i
}

func main() {
	fmt.Println(problem5(10))
	fmt.Println(problem5(20))
}
