-- Problem 6: Sum square difference

module Main where
squareSum n = sum [x^2 | x <- [1..n]]

sumSquare n = sum [1..n]^2

sumSquareDiff n = sumSquare n - squareSum n

main = do
  print answer
    where
      answer = sumSquareDiff 100
