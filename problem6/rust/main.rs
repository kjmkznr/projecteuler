fn square_sum(n: i32) -> i32 {
  match n {
    1 => 1,
    _ => square_sum(n-1) + n*n,
  }
}

fn sum_square(n: i32) -> i32 {
  let x = (1..n+1).fold(0, |n, x| n + x);
  return x*x
}

fn main() {
  println!("10 sum square diff = {}", sum_square(10) - square_sum(10));
  println!("100 sum square diff = {}", sum_square(100) - square_sum(100));
}
